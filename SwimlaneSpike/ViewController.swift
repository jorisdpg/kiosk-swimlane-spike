//
//  ViewController.swift
//  SwimlaneSpike
//
//  Created by Joris Van Gasteren on 16/10/2020.
//

import UIKit
import OpenCombine
import OpenCombineDispatch
import OpenCombineFoundation

class ViewController: UIViewController {

    private var cancellables = Set<AnyCancellable>()
 
//    private var viewModel: ViewModel!
    
    let label = UILabel()
    
    let fetcher: SwimlaneFetcher = SwimlaneFetcherImplementation()
    
    var swimlaneView: SwimlaneView {
        view as! SwimlaneView
    }

    override func loadView() {
        view = SwimlaneView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swimlaneView.viewModel = testViewModel
//        fetchSwimlanes()
    }
    
    private func fetchSwimlanes() {
        fetcher.fetchSwimlanes()
            .receive(on: DispatchQueue.OCombine(.main))
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    print("### finished")
                case .failure(let error):
                    print("### failed with error: \(error)")
                }
            }, receiveValue: {
                print("\($0)")
            })
            .store(in: &cancellables)
    }

//    private func setupBindings() {
//        subscriptions.insert(viewModel.$title.assign(to: \.text!, on: label))
//        subscriptions.insert(viewModel.$backgroundColor.assign(to: \.backgroundColor!, on: view))
//
//
//    }
}
