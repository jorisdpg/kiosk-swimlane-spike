//
//  ViewModels.swift
//  SwimlaneSpike
//
//  Created by Joris Van Gasteren on 16/10/2020.
//

import UIKit

struct SwimlaneViewModel {
    let sections: [SwimlaneSectionViewModel]
}

struct SwimlaneSectionViewModel {
    let title: String?
    let subtitle: String?
    let backgroundColor: UIColor?
    let items: [SwimlaneItemViewModel]
}

struct SwimlaneItemViewModel {
    let photo: UIImage?
    let icon: UIImage?
    let newspaper: String?
    let title: String?
}

let testSectionViewModel = SwimlaneSectionViewModel(
    title: "Goedemiddag!",
    subtitle: "Dit zijn de aanraders op basis van jouw voorkeuren",
    backgroundColor: UIColor(hexValue: 0xfeeded),
    items: [
        .init(
            photo: UIImage(named: "koeman"),
            icon: UIImage(named: "ad"),
            newspaper: "Algemeen Dagblad",
            title: "Erfenis Lionel Messi lust of last voor Koeman?"),
        .init(
            photo: UIImage(named: "rutte"),
            icon: UIImage(named: "vk"),
            newspaper: "de Volkskrant",
            title: "In crisistijd is het kabinet eerst populair, daarna volgt de afrekening"),
        .init(
            photo: UIImage(named: "hugo"),
            icon: UIImage(named: "trouw"),
            newspaper: "Trouw",
            title: "Zorgverzekeraars willen de marktwerking in 2021 terug"),
        .init(
            photo: UIImage(named: "cs"),
            icon: UIImage(named: "ad"),
            newspaper: "Algemeen Dagblad",
            title: "Al drie boetes van 50.000 euro voor vakantieverhuur"),
    ])

let testViewModel = SwimlaneViewModel(sections: [ testSectionViewModel,
    testSectionViewModel,
])
