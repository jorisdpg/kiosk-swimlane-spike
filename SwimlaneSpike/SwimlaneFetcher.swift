//
//  File.swift
//  
//
//  Created by Joris Van Gasteren on 20/10/2020.
//

import Foundation
import OpenCombine
import OpenCombineFoundation

protocol SwimlaneFetcher {
    func fetchSwimlanes() -> AnyPublisher<SwimlanesResponse, SwimlaneError>
}

class SwimlaneFetcherImplementation: SwimlaneFetcher {
    
    let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func fetchSwimlanes() -> AnyPublisher<SwimlanesResponse, SwimlaneError> {
        var request = URLRequest(url: URL(string: "https://private-d50e80-kiosk16.apiary-mock.com/swimlanes")!)
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        let decoder = JSONDecoder()

        return URLSession.OCombine(session).dataTaskPublisher(for: request)
            .map { $0.data }
            .decode(type: SwimlanesResponse.self, decoder: decoder)
            .mapError { SwimlaneError.error($0) }
            .eraseToAnyPublisher()
    }
    
}
