//
//  SwimlaneItemCell.swift
//  SwimlaneSpike
//
//  Created by Joris Van Gasteren on 23/10/2020.
//

import UIKit

final class SwimlaneItemCell: UICollectionViewCell {
    var viewModel: SwimlaneItemViewModel? {
        didSet {
            updateViewModel()
        }
    }

    let photoView = UIImageView()
    let iconView = UIImageView()
    let newspaperLabel = UILabel()
    let titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupCell() {
        backgroundColor = .white
        
        photoView.translatesAutoresizingMaskIntoConstraints = false
        photoView.contentMode = .scaleAspectFill
        addSubview(photoView)
        
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.contentMode = .center
        addSubview(iconView)
        
        newspaperLabel.translatesAutoresizingMaskIntoConstraints = false
        newspaperLabel.font = .preferredFont(forTextStyle: .caption1)
        newspaperLabel.textColor = .lightGray
        addSubview(newspaperLabel)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = .preferredFont(forTextStyle: .callout)
        titleLabel.numberOfLines = 0
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            photoView.topAnchor.constraint(equalTo: topAnchor),
            photoView.leadingAnchor.constraint(equalTo: leadingAnchor),
            photoView.trailingAnchor.constraint(equalTo: trailingAnchor),
            photoView.heightAnchor.constraint(equalToConstant: 148),
            iconView.topAnchor.constraint(equalTo: photoView.bottomAnchor, constant: -8),
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            iconView.widthAnchor.constraint(equalToConstant: 28),
            iconView.heightAnchor.constraint(equalToConstant: 28),
            newspaperLabel.topAnchor.constraint(equalTo: photoView.bottomAnchor, constant: 4),
            newspaperLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 4),
            newspaperLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            titleLabel.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: 8),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
        ])
    }
    
    private func updateViewModel() {
        photoView.image = viewModel?.photo
        iconView.image = viewModel?.icon
        newspaperLabel.text = viewModel?.newspaper
        titleLabel.text = viewModel?.title
    }
}

// MARK: - Previews

import SwiftUI

#if DEBUG
@available(iOS 13, *)
struct SwimlaneItemCellPreviews: PreviewProvider, UIViewRepresentable {
    
    let viewModel: SwimlaneItemViewModel
    
    // MARK: - PreviewProvider
    
    static var previews: some View {
        Self(viewModel: SwimlaneItemViewModel(photo: UIImage(named: "rutte"), icon: UIImage(named: "vk"), newspaper: "de Volkskrant", title: "In crisistijd is het kabinet eerst populair, daarna volgt de afrekening"))
            .previewLayout(.fixed(width: 155, height: 304))
    }
    
    // MARK: - UIViewRepresentable
    
    func makeUIView(context: Context) -> some UIView {
        let view = SwimlaneItemCell()
        view.viewModel = viewModel
        return view
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        // Update view with context
    }
}
#endif
