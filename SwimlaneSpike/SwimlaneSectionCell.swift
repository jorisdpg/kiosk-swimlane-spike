//
//  SwimlaneSectionCell.swift
//  SwimlaneSpike
//
//  Created by Joris Van Gasteren on 23/10/2020.
//

import UIKit

final class SwimlaneSectionCell: UITableViewCell {
    var viewModel: SwimlaneSectionViewModel? {
        didSet {
            updateViewModel()
        }
    }
    
    let itemCellReuseIdentifier = "ItemCell"
    
    let collectionViewLayout: UICollectionViewFlowLayout
    let collectionView: UICollectionView
    
    let titleLabel = UILabel()
    let subtitleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        collectionViewLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        viewModel = nil
        super.prepareForReuse()
    }
    
    private func setupCell() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = .preferredFont(forTextStyle: .headline)
        titleLabel.numberOfLines = 0
        contentView.addSubview(titleLabel)

        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.font = .preferredFont(forTextStyle: .subheadline)
        subtitleLabel.numberOfLines = 0
        subtitleLabel.textColor = .lightGray
        contentView.addSubview(subtitleLabel)
        
        collectionView.backgroundColor = .green
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(SwimlaneItemCell.self, forCellWithReuseIdentifier: itemCellReuseIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        contentView.addSubview(collectionView)
        
        collectionViewLayout.minimumInteritemSpacing = 8
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.sectionInset = .init(top: 8, left: 8, bottom: 8, right: 8)
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32),
            subtitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12),
            subtitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12),
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
            collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: 320)
        ])

    }
    
    private func updateViewModel() {
        titleLabel.text = viewModel?.title
        subtitleLabel.text = viewModel?.subtitle
        contentView.backgroundColor = viewModel?.backgroundColor
        collectionView.backgroundColor = viewModel?.backgroundColor
        collectionView.reloadData()
    }
}

extension SwimlaneSectionCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel?.items.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemCellReuseIdentifier, for: indexPath)
        if let cell = cell as? SwimlaneItemCell, let item = viewModel?.items[indexPath.row] {
            cell.viewModel = item
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 155, height: 304)
    }
}

import SwiftUI

#if DEBUG
@available(iOS 13, *)
struct SwimlaneSectionCellPreviews: PreviewProvider, UIViewRepresentable {
    
    let viewModel: SwimlaneSectionViewModel
    
    // MARK: - PreviewProvider
    
    static var previews: some View {
        Self(viewModel: testSectionViewModel)
            .previewLayout(.sizeThatFits)
    }
    
    // MARK: - UIViewRepresentable
    
    func makeUIView(context: Context) -> some UIView {
        let view = SwimlaneSectionCell()
        view.viewModel = viewModel
        return view
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        // Update view with context
    }
}
#endif

