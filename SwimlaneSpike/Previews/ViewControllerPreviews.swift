//
//  ViewControllerPreviews.swift
//  SwimlaneSpike
//
//  Created by Joris Van Gasteren on 21/10/2020.
//

import SwiftUI
import SwimlaneView

#if DEBUG
@available(iOS 13, *)
struct ViewControllerPreviews: PreviewProvider, UIViewControllerRepresentable {
    
    // MARK: - PreviewProvider

    static let previewDevices = [
        "iPhone SE (1st generation)",
        "iPhone SE (2nd generation)",
        "iPhone 8 Plus",
        "iPhone 11",
        "iPhone 12 mini",
        "iPhone 12",
        "iPhone 12 Pro Max",
    ]
    
    static var previews: some View {
        ForEach(previewDevices, id: \.self) { item in
            Self()
                .previewDevice(PreviewDevice(rawValue: item))
                .previewDisplayName(item)
        }
        Group {
            Self()
                .preferredColorScheme(.dark)
            Self()
                .environment(\.sizeCategory, ContentSizeCategory.extraSmall)
            Self()
                .environment(\.sizeCategory, ContentSizeCategory.extraExtraExtraLarge)
        }
    }

    // MARK: - UIViewControllerRepresentable

    func makeUIViewController(context: Context) -> some UIViewController {
        ViewController()
    }
 
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
    }
}
#endif
