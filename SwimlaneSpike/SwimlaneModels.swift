//
//  SwimlaneModels.swift
//  SwimlaneSpike
//
//  Created by Joris Van Gasteren on 29/10/2020.
//

import Foundation

struct SwimlanesResponse: Codable {
    let sections: [SwimlaneSection]
}

struct SwimlaneSection: Codable {
    struct Style: Codable {
        
    }
    struct Component: Codable {
        enum ComponentType: String, Codable {
            case header1 = "h1"
            case paragraph = "p"
            case swimlaneRow = "swimlane-row"
        }

        let type: ComponentType
        let text: String?
        let style: Style?
        let src: String?
        let height: String?
    }
    
    let style: Style?
    let components: [Component]
    
}

enum SwimlaneError: Error {
    case error(Error)
}
