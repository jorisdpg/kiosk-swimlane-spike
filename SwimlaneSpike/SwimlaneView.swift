import UIKit

final class SwimlaneView: UIView {
    var viewModel: SwimlaneViewModel? {
        didSet {
            updateViewModel()
        }
    }

    private let tableView = UITableView()
    
    private let sectionCellReuseIdentifier = "SectionCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .yellow
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(SwimlaneSectionCell.self, forCellReuseIdentifier: sectionCellReuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.topAnchor.constraint(equalTo:topAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    private func updateViewModel() {
        tableView.reloadData()
    }
}

extension SwimlaneView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.sections.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = viewModel!.sections[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: sectionCellReuseIdentifier, for: indexPath) as! SwimlaneSectionCell
        cell.viewModel = section
        return cell
    }
}


extension SwimlaneView: UITableViewDelegate {
    
}
