//
//  UIColor+Hex.swift
//  SwimlaneSpike
//
//  Created by Joris Van Gasteren on 23/10/2020.
//

import UIKit

extension UIColor {
    convenience init(hexValue: UInt32, alpha: CGFloat = 1.0) {
        let red = CGFloat((hexValue >> 16) & 0xff) / 255.0
        let green = CGFloat((hexValue >> 8) & 0xff) / 255.0
        let blue = CGFloat(hexValue & 0xff) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
